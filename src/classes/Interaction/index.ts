/*
 *  This file contains the class for an Interaction for Discord bots.
 *  Created On 11 May 2020
 */

import Discord, { BitFieldResolvable, PermissionString } from 'discord.js'

import { InteractionConfigImpl, InteractionArgumentImpl } from './interfaces'

export default class Interaction {
    data: InteractionConfigImpl = {
        command: null,
        description: null,
        restricts: [],
        action: null,
        arguments: [],
    }

    constructor(command: string) {
        this.data.command = command
    }

    description(desc: string): this {
        this.data.description = desc
        return this
    }

    restrict(identifiers: BitFieldResolvable<PermissionString>[]): this {
        this.data.restricts = identifiers
        return this
    }

    action(
        func: (message: Discord.Message, args: any) => Promise<boolean>,
    ): this {
        this.data.action = func
        return this
    }

    arguments(args: InteractionArgumentImpl[]): this {
        this.data.arguments = args
        return this
    }
}
