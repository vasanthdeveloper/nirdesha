import events from 'events'

import Discord from 'discord.js'

import NirdeshaAppConfigImpl from './interfaces'
import loadInteractions from './load'
import triggerInteractions from './trigger'

declare interface NirdeshaApp {
    on(event: string, listener: Function): this
    on(
        event: 'noInteraction',
        listener: (command: string, message: Discord.Message) => void,
    ): this
    on(
        event: 'interactionAccessDenied',
        listener: (command: string, message: Discord.Message) => void,
    ): this
    on(
        event: 'tooManyArguments',
        listener: (
            command: string,
            expected: number,
            got: number,
            message: Discord.Message,
        ) => void,
    ): this
    on(
        event: 'insufficientArguments',
        listener: (
            command: string,
            expected: number,
            got: number,
            message: Discord.Message,
        ) => void,
    ): this
}

class NirdeshaApp extends events.EventEmitter {
    data: NirdeshaAppConfigImpl = {
        name: null,
        client: null,
        prefix: ';',
        interactions: [],
        tasks: [],
        paths: {
            interactions: null,
            tasks: null,
        },
    }

    constructor(name: string, client: Discord.Client) {
        super()
        this.data.name = name
        this.data.client = client

        triggerInteractions(client, this)
    }

    async interactions(
        path: string,
        extension?: string,
        transformer?: (content: string) => Promise<string>,
    ): Promise<this> {
        this.data.paths.interactions = path
        this.data.interactions = await loadInteractions(
            path,
            extension,
            transformer,
        )
        return this
    }

    tasks(path: string): this {
        this.data.paths.tasks = path
        return this
    }
}

export default NirdeshaApp
