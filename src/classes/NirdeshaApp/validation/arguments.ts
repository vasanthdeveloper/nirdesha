/*
 *  This file will take in an argument array and check if they are good to go.
 *  In case there are in incorrect order or have invalid values, we throw an error.
 *  Created On 11 May 2020
 */

import Discord from 'discord.js'

import {
    InteractionConfigImpl,
    InteractionArgumentImpl,
} from '../../Interaction/interfaces'
import { forEach } from '../../../utilities/loops'
import NirdeshaApp from '..'

export default async function validateArguments(
    interaction: InteractionConfigImpl,
    command: string,
    app: NirdeshaApp,
    message: Discord.Message,
): Promise<any> {
    // convert the string into argument array we can use to validate
    const uArgs = command.substr(interaction.command.length).trim().split(' ')

    // remove extra empty string element
    if (uArgs.indexOf('') != -1) uArgs.splice(uArgs.indexOf(''), 1)

    // get number of required arguments
    const minimumArguments = interaction.arguments.filter(
        arg => arg.required == true,
    ).length

    // ensure minimum number of arguments were passed
    if (uArgs.length < minimumArguments) {
        app.emit(
            'insufficientArguments',
            interaction.command,
            minimumArguments,
            uArgs.length,
            message,
        )

        return
    }

    // the variable that stores returnable arguments
    const returnable = {}

    // loop through all the arguments and assign their values
    await forEach(
        interaction.arguments,
        async (arg: InteractionArgumentImpl) => {
            returnable[arg.name] = uArgs[0]
            uArgs.shift()
        },
    )

    // if there are still unparsed arguments
    // throw an exception
    if (uArgs.length > 0) {
        app.emit(
            'tooManyArguments',
            interaction.command,
            interaction.arguments.length,
            uArgs.length + interaction.arguments.length,
            message,
        )

        return
    }

    return returnable
}
