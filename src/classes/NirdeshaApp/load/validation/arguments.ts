/*
 *  This file will validate a command's arguments.
 *  Created On 12 August 2020
 */

import Interaction from '../../../Interaction'
import { InteractionArgumentImpl } from '../../../Interaction/interfaces'
import { forEach } from '../../../../utilities/loops'

export default async function args(
    code: Interaction,
    file: string,
    directory: string,
): Promise<void> {
    // check if there are any arguments
    if (code.data.arguments.length < 1) return

    // make sure that required arguments are defined
    // first, before defining optional ones
    await forEach(
        code.data.arguments,
        async (argument: InteractionArgumentImpl) => {
            // check if there is a name and that it doesn't
            // contain a space
            if (!argument.name)
                throw Error(
                    `The argument "${argument.identifier}" in "${file
                        .replace(directory, '')
                        .substring(1)}" doesn't have a name.`,
                )

            if (argument.name.includes(' '))
                throw Error(
                    `The argument "${argument.name}" in "${file
                        .replace(directory, '')
                        .substring(1)}" has invalid name.`,
                )

            // make sure there is a description
            if (!argument.description)
                throw Error(
                    `The argument "${argument.name}" in "${file
                        .replace(directory, '')
                        .substring(1)}" has no description.`,
                )

            // make sure there is an identifier
            if (!argument.identifier)
                throw Error(
                    `The argument "${argument.name}" in "${file
                        .replace(directory, '')
                        .substring(1)}" has no identifier.`,
                )

            // make sure there is a valid type
            if (!argument.type)
                throw Error(
                    `The argument "${argument.name}" in "${file
                        .replace(directory, '')
                        .substring(1)}" has no type.`,
                )

            const possibleTypes = ['string', 'number']
            if (!possibleTypes.includes(argument.type))
                throw Error(
                    `The argument "${argument.name}" in "${file
                        .replace(directory, '')
                        .substring(1)}" has invalid type.`,
                )

            // if there isn't a required key, it's should me made true by default
            if (argument.required == undefined) argument.required = true

            // now make sure there aren't required arguments after the
            // optional ones
            const nextElement =
                code.data.arguments[code.data.arguments.indexOf(argument) + 1]

            if (nextElement) {
                if (argument.required == false && nextElement.required == true)
                    throw Error(
                        `Cannot have required arguments after optional onces in "${file
                            .replace(directory, '')
                            .substring(1)}".`,
                    )
            }

            // if everything is valid go to the next element
            return
        },
    )

    // now that we finished loop through all elements
    // let's return back
    return
}
