/*
 *  This file reads a directory and require all the files and check if they export
 *  an expected class.
 *  Created On 11 May 2020
 */

import path from 'path'

import { sync as glob } from 'glob'

import Interaction from '../../Interaction'
import { forEach } from '../../../utilities/loops'
import transform from './transform'
import cmdValidation from './validation/index'
import argValidation from './validation/arguments'

export default async function load(
    directory: string,
    extension: string,
    transformer: (content: string) => Promise<string>,
): Promise<Interaction[]> {
    const files = glob(path.join(directory, '**', `*.${extension}`))
    const returnable: Interaction[] = []

    // loop through all the files in the interactions directory
    await forEach(files, async (file: string) => {
        // run it through our transformer
        const code = await transform(file, transformer)

        // validate if that command can be loaded
        await cmdValidation(code, file, directory)

        // validate all the arguments of that command
        await argValidation(code, file, directory)

        // add it to our list
        returnable.push(code)
    })

    return returnable
}
