/*
 *  This file will link the message event and trigger an interaction based on the command received.
 *  Created On 12 May 2020
 */

import Discord from 'discord.js'
import NirdeshaApp from '.'
import getInteraction from './validation/command'
import permission from './validation/permission'
import validateArguments from './validation/arguments'

export default async function interactionHookup(
    client: Discord.Client,
    app: NirdeshaApp,
): Promise<void> {
    client.on('message', async (message: Discord.Message) => {
        // check if the message starts with the prefix
        if (message.content.startsWith(app.data.prefix)) {
            // get the command
            const command = message.content.substring(app.data.prefix.length)

            // get an interaction which matches the command
            const interaction = await getInteraction(command, message, app)
            if (!interaction) return

            // now that we know there is an interaction tied to the
            // command, let's check if the user has permission or not
            const proceed = await permission(interaction, message, app)
            if (!proceed) return

            // finally let's validate that the user has given all required
            // arguments that the developer is expecting
            const args = await validateArguments(
                interaction.data,
                command,
                app,
                message,
            )
            if (!args) return

            // finally, let's launch the interaction's action
            interaction.data.action(message, args)
        }
    })
}
