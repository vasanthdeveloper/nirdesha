import Discord from 'discord.js';
import NirdeshaApp from '..';
import Interaction from '../../Interaction';
export default function matchInteraction(command: string, message: Discord.Message, app: NirdeshaApp): Promise<Interaction>;
//# sourceMappingURL=command.d.ts.map