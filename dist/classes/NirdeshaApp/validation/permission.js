"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const loops_1 = require("../../../utilities/loops");
function hasPerms(interaction, message, app) {
    return __awaiter(this, void 0, void 0, function* () {
        let hasAccess;
        yield loops_1.forEach(interaction.data.restricts, (role) => __awaiter(this, void 0, void 0, function* () {
            if (message.member.hasPermission(role)) {
                if (hasAccess !== true)
                    hasAccess = true;
            }
            else {
                hasAccess = false;
            }
        }));
        if (!hasAccess) {
            app.emit('interactionAccessDenied', interaction.data.command, message);
            return false;
        }
        else {
            return true;
        }
    });
}
exports.default = hasPerms;
