"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = __importDefault(require("./validation/command"));
const permission_1 = __importDefault(require("./validation/permission"));
const arguments_1 = __importDefault(require("./validation/arguments"));
function interactionHookup(client, app) {
    return __awaiter(this, void 0, void 0, function* () {
        client.on('message', (message) => __awaiter(this, void 0, void 0, function* () {
            if (message.content.startsWith(app.data.prefix)) {
                const command = message.content.substring(app.data.prefix.length);
                const interaction = yield command_1.default(command, message, app);
                if (!interaction)
                    return;
                const proceed = yield permission_1.default(interaction, message, app);
                if (!proceed)
                    return;
                const args = yield arguments_1.default(interaction.data, command, app, message);
                if (!args)
                    return;
                interaction.data.action(message, args);
            }
        }));
    });
}
exports.default = interactionHookup;
