"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const fs_1 = require("fs");
const glob_1 = require("glob");
const node_eval_1 = __importDefault(require("node-eval"));
const Interaction_1 = __importDefault(require("../Interaction"));
const loops_1 = require("../../utilities/loops");
function load(directory, extension, transformer) {
    return __awaiter(this, void 0, void 0, function* () {
        const files = glob_1.sync(path_1.default.join(directory, '**', `*.${extension}`));
        const returnable = [];
        const err = yield loops_1.forEach(files, (file) => __awaiter(this, void 0, void 0, function* () {
            const fileContents = (yield fs_1.promises.readFile(file, {
                encoding: 'UTF-8',
            }));
            let code;
            if (transformer) {
                const evalPath = path_1.default.format(Object.assign(Object.assign({}, path_1.default.parse(file)), { base: undefined, ext: '.js' }));
                code = node_eval_1.default(yield transformer(fileContents), evalPath).default;
            }
            else {
                code = require(file).default;
            }
            if (!(code instanceof Interaction_1.default)) {
                return Error(`The file "${file
                    .replace(directory, '')
                    .substring(1)}" doesn't export an Interaction.`);
            }
            if (!code.data.action) {
                return Error(`The file "${file
                    .replace(directory, '')
                    .substring(1)}" doesn't have an action tied to it.`);
            }
            if (code.data.arguments.length > 0) {
                yield loops_1.forEach(code.data.arguments, (argument) => __awaiter(this, void 0, void 0, function* () {
                    if (!argument.name)
                        throw Error(`The argument "${argument.identifier}" in "${file
                            .replace(directory, '')
                            .substring(1)}" doesn't have a name.`);
                    if (argument.name.includes(' '))
                        throw Error(`The argument "${argument.name}" in "${file
                            .replace(directory, '')
                            .substring(1)}" has invalid name.`);
                    if (!argument.description)
                        throw Error(`The argument "${argument.name}" in "${file
                            .replace(directory, '')
                            .substring(1)}" has no description.`);
                    if (!argument.identifier)
                        throw Error(`The argument "${argument.name}" in "${file
                            .replace(directory, '')
                            .substring(1)}" has no identifier.`);
                    if (!argument.type)
                        throw Error(`The argument "${argument.name}" in "${file
                            .replace(directory, '')
                            .substring(1)}" has no type.`);
                    const possibleTypes = ['string', 'number'];
                    if (!possibleTypes.includes(argument.type))
                        throw Error(`The argument "${argument.name}" in "${file
                            .replace(directory, '')
                            .substring(1)}" has invalid type.`);
                    if (argument.required == undefined)
                        argument.required = true;
                    const nextElement = code.data.arguments[code.data.arguments.indexOf(argument) + 1];
                    if (nextElement) {
                        if (argument.required == false &&
                            nextElement.required == true)
                            throw Error(`Cannot have required arguments after optional onces in "${file
                                .replace(directory, '')
                                .substring(1)}".`);
                    }
                    return;
                }));
            }
            returnable.push(code);
        }));
        if (err)
            throw err;
        return returnable;
    });
}
exports.default = load;
