"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = __importDefault(require("events"));
const load_1 = __importDefault(require("./load"));
const trigger_1 = __importDefault(require("./trigger"));
class NirdeshaApp extends events_1.default.EventEmitter {
    constructor(name, client) {
        super();
        this.data = {
            name: null,
            client: null,
            prefix: ';',
            interactions: [],
            tasks: [],
            paths: {
                interactions: null,
                tasks: null,
            },
        };
        this.data.name = name;
        this.data.client = client;
        trigger_1.default(client, this);
    }
    interactions(path, extension, transformer) {
        return __awaiter(this, void 0, void 0, function* () {
            this.data.paths.interactions = path;
            this.data.interactions = yield load_1.default(path, extension, transformer);
            return this;
        });
    }
    tasks(path) {
        this.data.paths.tasks = path;
        return this;
    }
}
exports.default = NirdeshaApp;
