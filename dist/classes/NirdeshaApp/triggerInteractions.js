"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const loops_1 = require("../../utilities/loops");
const validateArguments_1 = __importDefault(require("./validateArguments"));
function canTrigger(interaction, message) {
    return __awaiter(this, void 0, void 0, function* () {
        let hasAccess;
        yield loops_1.forEach(interaction.data.restricts, (role) => __awaiter(this, void 0, void 0, function* () {
            if (message.member.hasPermission(role)) {
                if (hasAccess !== true)
                    hasAccess = true;
            }
            else {
                hasAccess = false;
            }
        }));
        if (!hasAccess) {
            return false;
        }
        else {
            return true;
        }
    });
}
function interactionHookup(client, app) {
    return __awaiter(this, void 0, void 0, function* () {
        client.on('message', (message) => __awaiter(this, void 0, void 0, function* () {
            if (message.content.startsWith(app.data.prefix)) {
                const command = message.content.substring(app.data.prefix.length);
                const commandSplit = command.split(' ');
                let currentIteration = commandSplit.length;
                let interaction;
                if (commandSplit.length > 1) {
                    do {
                        const commandStr = commandSplit
                            .slice(0, currentIteration)
                            .join(' ');
                        interaction = app.data.interactions.find(inter => inter.data.command.startsWith(commandStr));
                        currentIteration--;
                    } while (currentIteration > 0 || !interaction);
                }
                else {
                    interaction = app.data.interactions.find(inter => inter.data.command.startsWith(command));
                }
                if (!interaction) {
                    app.emit('noInteraction', command, message);
                }
                else {
                    const proceed = yield canTrigger(interaction, message);
                    if (proceed) {
                        const args = yield validateArguments_1.default(interaction.data, command, app, message);
                        if (!args)
                            return;
                        interaction.data.action(message, args);
                    }
                    else {
                        app.emit('interactionAccessDenied', interaction.data.command, message);
                    }
                }
            }
        }));
    });
}
exports.default = interactionHookup;
