"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Interaction {
    constructor(command) {
        this.data = {
            command: null,
            description: null,
            restricts: [],
            action: null,
            arguments: [],
        };
        this.data.command = command;
    }
    description(desc) {
        this.data.description = desc;
        return this;
    }
    restrict(identifiers) {
        this.data.restricts = identifiers;
        return this;
    }
    action(func) {
        this.data.action = func;
        return this;
    }
    arguments(args) {
        this.data.arguments = args;
        return this;
    }
}
exports.default = Interaction;
