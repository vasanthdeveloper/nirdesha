"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const NirdeshaApp_1 = __importDefault(require("./classes/NirdeshaApp"));
const index_1 = __importDefault(require("./classes/Interaction/index"));
function app(name, client) {
    return __awaiter(this, void 0, void 0, function* () {
        return new NirdeshaApp_1.default(name, client);
    });
}
exports.default = {
    app,
    Interaction: index_1.default,
};
var index_2 = require("./classes/Interaction/index");
exports.Interaction = index_2.default;
